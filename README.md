
# User Registration APP

SPA responsável por gerenciar usuários

## Sumário

- [Pré-requisitos](#pré-requisitos)
- [Executar localmente](#executar-localmente)
- [Variáveis de ambiente](#variáveis-de-ambiente)
- [Demonstração](#demonstração)
- [Testes](#rodando-os-testes)

## Pré-requisitos

- [Node.js >= 12.18.0](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)
- [NVM](https://github.com/nvm-sh/nvm)

## Executar localmente

Faça um clone do projeto

#### https
```bash
  git clone https://gitlab.com/fernandohfs94/user-registration-app
```

#### ssh
```bash
  git clone git@gitlab.com:fernandohfs94/user-registration-app.git
```

Vá para o diretório do projeto

```bash
  cd user-registration-app
```

Instale e use a versão `12.18.0` do `nodejs`

```bash
nvm install 12.18.0
```

Instale as dependências

```bash
  yarn
```

Crie um arquivo `.env.development` com as variáveis de ambiente preenchidas (de acordo com o arquivo `.env.example`)

Inicie o projeto

```bash
  yarn start
```

## Variáveis de ambiente

Para executar este projeto, você precisará adicionar as seguintes variáveis de ambiente ao seu arquivo `.env`

| Nome | Descrição | Valor Padrão | Exemplo | Obrigatório |
| -- | -- | -- | -- | -- |
| REACT_APP_ENVIRONMENT | Ambiente em que o app está rodando (development, staging ou production) | | development | :white_check_mark: |
| REACT_APP_USER_REGISTRATION_API_URL | URL da API user-registration-api | | https://user-registration-api-fernando.herokuapp.com/v1 | :white_check_mark: |
| REACT_APP_USER_REGISTRATION_API_TOKEN | Token JWT da API user-registration-api | | | :white_check_mark: |
| REACT_APP_USER_REGISTRATION_API_TIMEOUT | Timeout (em milissegundos) de request para a API user-registration-api | 30000 |  | |
| REACT_APP_VIA_CEP_URL | URL da API Via Cep | | https://viacep.com.br/ws | :white_check_mark: |
| REACT_APP_VIA_CEP_TIMEOUT | Timeout (em milissegundos) de request para a API Via Cep | 30000 | | |

## Demonstração

Para ver e usar a aplicação em produção, basta acessar o link [https://user-registration-app.netlify.app/](https://user-registration-app.netlify.app)

## Rodando os testes

Para rodar os testes, execute o seguinte comando na pasta raiz do projeto

```bash
  yarn test:ci
```

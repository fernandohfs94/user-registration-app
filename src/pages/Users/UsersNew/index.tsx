import { Container } from 'react-grid-system';

import Paths from '../../../constants/paths.constant';

import Layout from '../../../containers/Layout';
import PageHeader from '../../../components/PageHeader';
import Breadcrumbs from '../../../components/Breadcrumbs';
import BreadcrumbItem from '../../../components/Breadcrumbs/BreadcrumbItem';
import FormUsers from '../../../containers/Form/FormUsers';

function UsersNew() {
  return (
    <Layout>
      <Container>
        <PageHeader title="Cadastrar um novo usuário" />
        <Breadcrumbs>
          <BreadcrumbItem path={Paths.Home} title="Dashboard" />
          <BreadcrumbItem path={Paths.Users.Root} title="Usuários" />
          <BreadcrumbItem title="Cadastrar" />
        </Breadcrumbs>

        <FormUsers />
      </Container>
    </Layout>
  );
}

export default UsersNew;

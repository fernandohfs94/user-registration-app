import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import UsersNew from '../index';

describe('UsersNew page', () => {
  it('should be able to render UsersNew successfully', () => {
    const wrapper = mount(
      <Router>
        <UsersNew />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

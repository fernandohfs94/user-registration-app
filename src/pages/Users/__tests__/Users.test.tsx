import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';
import { render, fireEvent, act } from '@testing-library/react';

import { useUser } from '../../../hooks/useUser';
import { show } from '../../../utils/sweet-alert.util';
import { usersPageMock } from '../__mocks__/Users.mock';

import Users from '../index';

jest.mock('../../../hooks/useUser');
jest.mock('../../../utils/sweet-alert.util');

describe('Users page', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be able to render Users page successfully', () => {
    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      fetchUsersByFilters: jest.fn(),
      loading: false,
      error: false,
      users: [],
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render Users page with a list of users succesfully', () => {
    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      fetchUsersByFilters: jest.fn(),
      loading: true,
      error: false,
      users: usersPageMock,
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render Users page with an alert when there arent users and loading is false', () => {
    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      fetchUsersByFilters: jest.fn(),
      loading: false,
      error: false,
      users: [],
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render Users page with an alert type error when occurs one', () => {
    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      fetchUsersByFilters: jest.fn(),
      loading: false,
      error: true,
      users: [],
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to handle delete user button click successfully', () => {
    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      deleteUser: jest.fn(),
      fetchUsersByFilters: jest.fn(),
      loading: false,
      loadingDelete: false,
      error: false,
      users: usersPageMock,
    });

    (show as unknown as jest.Mock).mockResolvedValue({
      isConfirmed: true,
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>,
    );

    const deleteButton = wrapper.find('button').at(1);

    deleteButton.simulate('click');

    expect(show).toHaveBeenCalled();
  });

  it('should not be able to delete an user when is not confimed by alert', () => {
    const deleteUserMock = jest.fn();

    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      deleteUser: deleteUserMock,
      fetchUsersByFilters: jest.fn(),
      loading: false,
      loadingDelete: false,
      error: false,
      users: usersPageMock,
    });

    (show as unknown as jest.Mock).mockResolvedValue({
      isConfirmed: false,
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>,
    );

    const deleteButton = wrapper.find('button').at(1);

    deleteButton.simulate('click');

    expect(show).toHaveBeenCalled();
    expect(deleteUserMock).not.toHaveBeenCalled();
  });

  it('should be able show loading of delete successfully', () => {
    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      deleteUser: jest.fn(),
      fetchUsersByFilters: jest.fn(),
      loading: false,
      loadingDelete: true,
      error: false,
      users: usersPageMock,
    });

    (show as unknown as jest.Mock).mockResolvedValue({
      isConfirmed: false,
    });

    const wrapper = mount(
      <Router>
        <Users />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to handle filter button click successfully', async () => {
    const fetchUsersByFiltersMock = jest.fn();

    (useUser as unknown as jest.Mock).mockReturnValue({
      getUsers: jest.fn(),
      deleteUser: jest.fn(),
      fetchUsersByFilters: fetchUsersByFiltersMock,
      loading: false,
      loadingDelete: false,
      error: false,
      users: usersPageMock,
    });

    (show as unknown as jest.Mock).mockResolvedValue({
      isConfirmed: false,
    });

    const { container } = render(
      <Router>
        <Users />
      </Router>,
    );

    const inputFullName = container.querySelector(
      "input[name='fullName']",
    ) as HTMLInputElement;
    const inputLogin = container.querySelector(
      "input[name='login']",
    ) as HTMLInputElement;
    const inputDocument = container.querySelector(
      "input[name='document']",
    ) as HTMLInputElement;

    fireEvent.change(inputFullName, { target: { value: 'Testing' } });
    fireEvent.change(inputLogin, { target: { value: 'tt_test' } });
    fireEvent.change(inputDocument, { target: { value: '12345678901' } });

    const filterButton = container.querySelectorAll(
      'button',
    )[0] as HTMLButtonElement;

    await act(async () => {
      fireEvent.click(filterButton);
    });

    expect(fetchUsersByFiltersMock).toHaveBeenCalledWith(
      'Testing',
      'tt_test',
      '12345678901',
    );
  });
});

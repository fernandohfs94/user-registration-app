import { Container } from 'react-grid-system';

import { useEffect } from 'react';
import { useParams } from 'react-router-dom';

import Paths from '../../../constants/paths.constant';

import Breadcrumbs from '../../../components/Breadcrumbs';
import BreadcrumbItem from '../../../components/Breadcrumbs/BreadcrumbItem';
import PageHeader from '../../../components/PageHeader';
import Loading from '../../../components/Loading';
import Layout from '../../../containers/Layout';

import { useUser } from '../../../hooks/useUser';
import FormUsers from '../../../containers/Form/FormUsers';

function UsersEdit() {
  const { getOneUser, loading, user } = useUser();
  const { uuid } = useParams<{ uuid: string }>();

  useEffect(() => {
    getOneUser(uuid);
  }, [getOneUser, uuid]);

  if (loading) return <Loading />;

  return (
    <Layout>
      <Container>
        <PageHeader title="Cadastrar um novo usuário" />
        <Breadcrumbs>
          <BreadcrumbItem path={Paths.Home} title="Dashboard" />
          <BreadcrumbItem path={Paths.Users.Root} title="Usuários" />
          <BreadcrumbItem title="Atualizar" />
        </Breadcrumbs>

        <FormUsers
          initialData={{
            login: user.login,
            document: user.document,
            fullName: user.fullName,
            address: user.address,
          }}
          uuid={uuid}
        />
      </Container>
    </Layout>
  );
}

export default UsersEdit;

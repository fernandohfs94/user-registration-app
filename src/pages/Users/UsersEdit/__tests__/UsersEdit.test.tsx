import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import UsersEdit from '../index';

describe('UsersEdit page', () => {
  it('should be able to render UsersEdit successfully', () => {
    const wrapper = mount(
      <Router>
        <UsersEdit />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

import { Col, Container, Row } from 'react-grid-system';
import { useEffect, useCallback, useState } from 'react';
import { Link } from 'react-router-dom';
import { FaEdit, FaTrash } from 'react-icons/fa';

import Paths from '../../constants/paths.constant';
import Color from '../../styles/color';

import PageHeader from '../../components/PageHeader';
import Breadcrumbs from '../../components/Breadcrumbs';
import BreadcrumbItem from '../../components/Breadcrumbs/BreadcrumbItem';
import Layout from '../../containers/Layout';
import PageContent from '../../components/PageContent';
import Loader from '../../components/Loader';
import Alert from '../../components/Alert';
import Table from '../../components/Table';
import Loading from '../../components/Loading';
import FloatingButton from '../../components/FloatingButton';
import Button from '../../components/Button';
import ButtonBox from '../../components/ButtonBox';

import { show } from '../../utils/sweet-alert.util';
import { formatDocument } from '../../utils/format-document.util';

import { useUser } from '../../hooks/useUser';

import { Input } from '../../containers/Form/styles';
import { DeleteButton } from './styles';

function Users() {
  const [inputFullName, setInputFullName] = useState<string>('');
  const [inputLogin, setInputLogin] = useState<string>('');
  const [inputDocument, setInputDocument] = useState<string>('');

  const {
    getUsers,
    fetchUsersByFilters,
    deleteUser,
    loading,
    loadingDelete,
    error,
    users,
  } = useUser();

  const handleFullNameChange = useCallback(event => {
    setInputFullName(event.target.value);
  }, []);

  const handleLoginChange = useCallback(event => {
    setInputLogin(event.target.value);
  }, []);

  const handleDocumentChange = useCallback(event => {
    setInputDocument(event.target.value);
  }, []);

  const handleFilterButtonClick = useCallback(async () => {
    await fetchUsersByFilters(inputFullName, inputLogin, inputDocument);
  }, [inputFullName, inputLogin, inputDocument, fetchUsersByFilters]);

  const handleDeleteUserClick = useCallback(
    async (uuid: string) => {
      const { isConfirmed } = await show({
        type: 'warning',
        title: 'Tem certeza que quer excluir o usuário?',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonText: 'Sim, excluir usuário',
        cancelButtonText: 'Cancelar',
      });

      if (isConfirmed) {
        await deleteUser(uuid);
      }
    },
    [deleteUser],
  );

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  if (loadingDelete) return <Loading />;

  return (
    <>
      <Layout>
        <Container>
          <PageHeader title="Usuários" />

          <Breadcrumbs>
            <BreadcrumbItem path={Paths.Home} title="Dashboard" />
            <BreadcrumbItem title="Usuários" />
          </Breadcrumbs>

          <PageContent>
            <Row>
              <Col md={6} lg={3}>
                <Input
                  name="fullName"
                  onChange={handleFullNameChange}
                  placeholder="Digite o nome completo para filtrar"
                  value={inputFullName}
                />
              </Col>
              <Col md={6} lg={3}>
                <Input
                  name="login"
                  onChange={handleLoginChange}
                  placeholder="Digite o login para filtrar"
                  value={inputLogin}
                />
              </Col>
              <Col md={6} lg={3}>
                <Input
                  name="document"
                  onChange={handleDocumentChange}
                  placeholder="Digite o CPF para filtrar"
                  value={inputDocument}
                />
              </Col>
              <Col md={6} lg={3}>
                <ButtonBox>
                  <Button onClick={handleFilterButtonClick} fluid>
                    Filtrar
                  </Button>
                </ButtonBox>
              </Col>
            </Row>

            {loading && <Loader text="Carregando os usuários" />}

            {error && (
              <Alert type="error">
                Ooops... Ocorreu um erro ao tentar buscar os usuários. <br />{' '}
                Tente novamente mais tarde.
              </Alert>
            )}

            {!error && users.length === 0 && !loading && (
              <>
                <Alert type="info">
                  Nenhum usuário cadastrado no sistema. Cadastre um usuário
                  clicando <Link to={Paths.Users.New}>aqui</Link>.
                </Alert>
              </>
            )}

            {users.length > 0 && (
              <Table>
                <Table.Head>
                  <Table.Row>
                    <Table.HeadCell>Nome</Table.HeadCell>
                    <Table.HeadCell>Login</Table.HeadCell>
                    <Table.HeadCell>CPF</Table.HeadCell>
                    <Table.HeadCell>Ações</Table.HeadCell>
                  </Table.Row>
                </Table.Head>
                <Table.Body>
                  {users.map(({ id, uuid, login, fullName, document }) => (
                    <Table.Row key={id}>
                      <Table.Cell>{login}</Table.Cell>
                      <Table.Cell>{fullName}</Table.Cell>
                      <Table.Cell>{formatDocument(document)}</Table.Cell>
                      <Table.Cell>
                        <Link
                          to={Paths.Users.Edit.replace(':uuid', uuid)}
                          className="action-icon"
                        >
                          <FaEdit size={20} color={Color.primary} />
                        </Link>
                        <DeleteButton
                          type="button"
                          className="action-icon"
                          onClick={() => handleDeleteUserClick(uuid)}
                        >
                          <FaTrash size={20} color={Color.error.default} />
                        </DeleteButton>
                      </Table.Cell>
                    </Table.Row>
                  ))}
                </Table.Body>
              </Table>
            )}
          </PageContent>
        </Container>
      </Layout>
      <FloatingButton path={Paths.Users.New} />
    </>
  );
}

export default Users;

import styled from 'styled-components';

export const DeleteButton = styled.button`
  margin: 0;
  padding: 0;
  outline: 0;
  background: transparent;
  border: none;
`;

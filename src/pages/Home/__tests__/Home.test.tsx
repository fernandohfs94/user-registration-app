import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import Home from '../index';

describe('Home page', () => {
  it('should be able to render Home page successfully', () => {
    const wrapper = mount(
      <Router>
        <Home />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

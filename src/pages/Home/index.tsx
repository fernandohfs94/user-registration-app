import { Container, Row, Col } from 'react-grid-system';
import { FaUser } from 'react-icons/fa';

import PageHeader from '../../components/PageHeader';
import Layout from '../../containers/Layout';

import Paths from '../../constants/paths.constant';

import { Card } from './styles';

function Home() {
  return (
    <Layout>
      <Container>
        <PageHeader title="Dashboard" />
        <Row>
          <Col xs={12} sm={6} lg={3}>
            <Card to={Paths.Users.Root}>
              <FaUser size={60} />
              <p>Usuários</p>
            </Card>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
}

export default Home;

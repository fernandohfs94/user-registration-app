import { Link } from 'react-router-dom';

import styled from 'styled-components';

import Color from '../../styles/color';

export const Card = styled(Link)`
  width: 100%;
  background: ${Color.white.default};
  color: ${Color.primary120};
  padding: 1.5rem 0;
  border-radius: 0.375rem;
  box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.08);
  text-align: center;
  text-decoration: none;
  margin-top: 1.5rem;
  margin-right: 1.5rem;
  min-height: 156px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  svg {
    margin-bottom: 0.5rem;
  }

  p {
    font-size: 1.3rem;
  }

  &:hover {
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
    opacity: 0.8;
  }
`;

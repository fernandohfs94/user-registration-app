import Routes from './routes';

import { GlobalStyle } from './styles/global.styles';

function App() {
  return (
    <>
      <Routes />
      <GlobalStyle />
    </>
  );
}

export default App;

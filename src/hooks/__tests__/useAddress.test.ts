import MockAdapter from 'axios-mock-adapter';
import { renderHook, act } from '@testing-library/react-hooks';

import ViaCepAPI from '../../requests/via-cep/base.request';
import { viaCepResultMock } from '../__mocks__/useAddress.mock';

import { useAddress } from '../useAddress';

const mockAdapter = new MockAdapter(ViaCepAPI);

describe('useAddress hook', () => {
  const zipCode = '14412206';

  afterEach(() => {
    mockAdapter.reset();
  });

  describe('searchAddressByZipCode(zipCode)', () => {
    it('should be able to search an address with zip code successfully', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useAddress());

      mockAdapter.onGet(`/${zipCode}/json`).reply(200, viaCepResultMock);

      act(() => {
        result.current.searchAddressByZipCode(zipCode);
      });

      await waitForNextUpdate();

      expect(result.current.error).toBe(false);
    });

    it('should be able return an empty object when occurs an error when trying to search address by zip code', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useAddress());

      mockAdapter.onGet(`/${zipCode}/json`).reply(500);

      act(() => {
        result.current.searchAddressByZipCode(zipCode);
      });

      await waitForNextUpdate();

      expect(result.current.error).toBe(true);
    });
  });

  describe('getStates()', () => {
    it('should be able to get states successfully', async () => {
      const { result } = renderHook(() => useAddress());

      act(() => {
        result.current.getStates();
      });

      expect(result.current.states.length).toBeGreaterThan(0);
    });
  });
});

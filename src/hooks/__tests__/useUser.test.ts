import MockAdapter from 'axios-mock-adapter';
import { renderHook, act } from '@testing-library/react-hooks';

import API from '../../requests/user-registration-api/base.request';
import Paths from '../../constants/paths.constant';
import { userRequestMock, usersMock } from '../__mocks__/useUser.mock';
import { IUserRequest } from '../../requests/user-registration-api/users/interfaces/users.interface';
import { show } from '../../utils/sweet-alert.util';

import { useUser } from '../useUser';

const mockAdapter = new MockAdapter(API);
const mockedPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockedPush,
  }),
}));

jest.mock('../../utils/sweet-alert.util');

describe('useUser hook', () => {
  afterEach(() => {
    mockAdapter.reset();
  });

  describe('getUsers()', () => {
    it('should be able to get Users successfully', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onGet('/users').reply(200, usersMock);

      act(() => {
        result.current.getUsers();
      });

      await waitForNextUpdate();

      expect(result.current.users).toEqual(usersMock);
      expect(result.current.error).toBe(false);
    });

    it('should be able set error true when occurs one on get Users', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onGet('/users').reply(500);

      act(() => {
        result.current.getUsers();
      });

      await waitForNextUpdate();

      expect(result.current.users.length).toBe(0);
      expect(result.current.error).toBe(true);
    });
  });

  describe('getOneUser(uuid)', () => {
    const uuid = 'adbe514b-ee76-4c4f-bb5c-d42819e6a7b8';

    it('should be able to get one user successfully', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onGet(`/users/${uuid}`).reply(200, usersMock[0]);

      act(() => {
        result.current.getOneUser(uuid);
      });

      await waitForNextUpdate();

      expect(result.current.user).toEqual(usersMock[0]);
      expect(result.current.error).toBe(false);
    });

    it('should be able set error true when occurs one when trying to get an user', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onGet(`/users/${uuid}`).reply(500);

      act(() => {
        result.current.getOneUser(uuid);
      });

      await waitForNextUpdate();

      expect(result.current.user).toEqual({});
      expect(result.current.error).toBe(true);
    });
  });

  describe('createUser(data)', () => {
    it('should be able to add new User successfully', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onPost('/users').reply(201, usersMock[0]);

      act(() => {
        result.current.createUser(userRequestMock as IUserRequest);
      });

      await waitForNextUpdate();

      expect(show).toHaveBeenCalledWith({
        type: 'success',
        title: 'Usuário cadastrado com sucesso!',
        timer: 2000,
      });
      expect(mockedPush).toHaveBeenCalledWith(Paths.Users.Root);
    });

    it('should be able to handle conflict error when add a new User', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onPost('/users').reply(409);

      act(() => {
        result.current.createUser(userRequestMock as IUserRequest);
      });

      await waitForNextUpdate();

      expect(show).toHaveBeenCalledWith({
        type: 'error',
        title: 'Usuário já existe',
        text: 'Você tentou cadastrar um usuário que já existe com esse login ou CPF.',
        showConfirmButton: true,
      });
      expect(mockedPush).not.toBeCalled();
    });

    it('should be able to handle others errors when add a new User', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onPost('/users').reply(500);

      act(() => {
        result.current.createUser(userRequestMock as IUserRequest);
      });

      await waitForNextUpdate();

      expect(show).toHaveBeenCalledWith({
        type: 'error',
        title: 'Ooops...',
        text: 'Ocorreu um erro ao tentar cadastrar um novo usuário.\nTente novamente mais tarde.',
        showConfirmButton: true,
      });
      expect(mockedPush).not.toBeCalled();
    });
  });

  describe('updateUser(data, uuid)', () => {
    const uuid = 'adbe514b-ee76-4c4f-bb5c-d42819e6a7b8';

    it('should be able to update an user successfully', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onPatch(`/users/${uuid}`).reply(200, usersMock[0]);

      act(() => {
        result.current.updateUser(userRequestMock as IUserRequest, uuid);
      });

      await waitForNextUpdate();

      expect(show).toHaveBeenCalledWith({
        type: 'success',
        title: 'Usuário atualizado com sucesso!',
        timer: 2000,
      });
      expect(mockedPush).toHaveBeenCalledWith(Paths.Users.Root);
    });

    it('should be able to handle an error and show alert when occurs one', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onPatch(`/users/${uuid}`).reply(500);

      act(() => {
        result.current.updateUser(userRequestMock as IUserRequest, uuid);
      });

      await waitForNextUpdate();

      expect(show).toHaveBeenCalledWith({
        type: 'error',
        title: 'Ooops...',
        text: 'Ocorreu um erro ao tentar cadastrar um novo usuário.\nTente novamente mais tarde.',
        showConfirmButton: true,
      });
      expect(mockedPush).not.toHaveBeenCalledWith(Paths.Users.Root);
    });
  });

  describe('deleteUser(uuid)', () => {
    const uuid = 'adbe514b-ee76-4c4f-bb5c-d42819e6a7b8';

    it('should be able to delete an user successfully', async () => {
      (show as unknown as jest.Mock).mockResolvedValue({});

      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onDelete(`/users/${uuid}`).reply(204);

      act(() => {
        result.current.deleteUser(uuid);
      });

      await waitForNextUpdate();

      expect(show).toHaveBeenCalledWith({
        type: 'success',
        title: 'Usuário excluído com sucesso!',
        timer: 2500,
      });
    });

    it('should be able to handle error when delete an User', async () => {
      (show as unknown as jest.Mock).mockResolvedValue({});

      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onDelete(`/users/${uuid}`).reply(500);

      act(() => {
        result.current.deleteUser(uuid);
      });

      await waitForNextUpdate();

      expect(show).toHaveBeenCalledWith({
        type: 'error',
        title: 'Ooops...',
        text: 'Ocorreu um erro ao tentar excluir o usuário.\nTente novamente mais tarde.',
        showConfirmButton: true,
      });
    });
  });

  describe('getUsers()', () => {
    const fullName = 'Testing';

    it('should be able to fetch users by filters successfully', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onGet(`/users?fullName=${fullName}`).reply(200, usersMock);

      act(() => {
        result.current.fetchUsersByFilters(fullName);
      });

      await waitForNextUpdate();

      expect(result.current.users).toEqual(usersMock);
      expect(result.current.error).toBe(false);
    });

    it('should be able set error true when occurs one on get Users', async () => {
      const { result, waitForNextUpdate } = renderHook(() => useUser());

      mockAdapter.onGet(`/users?fullName=${fullName}`).reply(500);

      act(() => {
        result.current.fetchUsersByFilters(fullName);
      });

      await waitForNextUpdate();

      expect(result.current.users.length).toBe(0);
      expect(result.current.error).toBe(true);
    });
  });
});

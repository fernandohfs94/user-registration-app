import { useState, useCallback } from 'react';

import { statesMock } from './__mocks__/useAddress.mock';
import { ICepResponse } from '../requests/via-cep/cep/interfaces/cep.interface';
import { getAddressByZipCode } from '../requests/via-cep/cep/cep.request';

type State = {
  description: string;
  value: string;
};

interface useAddressResponse {
  searchAddressByZipCode(zipCode: string): Promise<ICepResponse>;
  getStates(): void;
  loading: boolean;
  error: boolean;
  states: State[];
}

export const useAddress = (): useAddressResponse => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [states, setStates] = useState<State[]>([]);

  const searchAddressByZipCode = useCallback(
    async (zipCode: string): Promise<ICepResponse> => {
      try {
        setError(false);
        setLoading(true);

        const response = await getAddressByZipCode(zipCode);

        return response.data;
      } catch (err) {
        setError(true);
      } finally {
        setLoading(false);
      }

      return {} as ICepResponse;
    },
    [],
  );

  const getStates = useCallback(() => {
    setStates(statesMock);
  }, []);

  return {
    searchAddressByZipCode,
    getStates,
    loading,
    error,
    states,
  };
};

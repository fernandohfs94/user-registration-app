import { useState, useCallback } from 'react';
import { useHistory } from 'react-router-dom';

import Paths from '../constants/paths.constant';
import {
  IUserRequest,
  IUserResponse,
} from '../requests/user-registration-api/users/interfaces/users.interface';
import {
  addNewUser,
  deleteUserById,
  getAllUsers,
  getUsersByFilters,
  getUserByUuid,
  updateUserByUuid,
} from '../requests/user-registration-api/users/users.request';

import { show } from '../utils/sweet-alert.util';

interface useUserResponse {
  getUsers(): Promise<void>;
  getOneUser(uuid: string): Promise<void>;
  createUser(data: IUserRequest): Promise<void>;
  updateUser(data: IUserRequest, uuid: string): Promise<void>;
  deleteUser(id: string): Promise<void>;
  fetchUsersByFilters(
    fullName?: string,
    login?: string,
    document?: string,
  ): Promise<void>;
  loading: boolean;
  loadingDelete: boolean;
  error: boolean;
  user: IUserResponse;
  users: IUserResponse[];
}

export const useUser = (): useUserResponse => {
  const [loading, setLoading] = useState<boolean>(false);
  const [loadingDelete, setLoadingDelete] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [user, setUser] = useState<IUserResponse>({} as IUserResponse);
  const [users, setUsers] = useState<IUserResponse[]>([]);

  const { push } = useHistory();

  const getUsers = useCallback(async () => {
    try {
      setError(false);
      setLoading(true);

      const response = await getAllUsers();

      setUsers(response.data);
    } catch (err) {
      setError(true);
    } finally {
      setLoading(false);
    }
  }, []);

  const getOneUser = useCallback(async (uuid: string) => {
    try {
      setError(false);
      setLoading(true);

      const response = await getUserByUuid(uuid);

      setUser(response.data);
    } catch (err) {
      setError(true);
    } finally {
      setLoading(false);
    }
  }, []);

  const createUser = useCallback(
    async (data: IUserRequest) => {
      try {
        setLoading(true);

        await addNewUser(data);

        show({
          type: 'success',
          title: 'Usuário cadastrado com sucesso!',
          timer: 2000,
        });

        push(Paths.Users.Root);
      } catch (err) {
        if (err.response && err.response.status === 409) {
          show({
            type: 'error',
            title: 'Usuário já existe',
            text: 'Você tentou cadastrar um usuário que já existe com esse login ou CPF.',
            showConfirmButton: true,
          });
        } else {
          show({
            type: 'error',
            title: 'Ooops...',
            text: 'Ocorreu um erro ao tentar cadastrar um novo usuário.\nTente novamente mais tarde.',
            showConfirmButton: true,
          });
        }
      } finally {
        setLoading(false);
      }
    },
    [push],
  );

  const updateUser = useCallback(
    async (data: IUserRequest, uuid: string) => {
      try {
        setLoading(true);

        await updateUserByUuid(data, uuid);

        show({
          type: 'success',
          title: 'Usuário atualizado com sucesso!',
          timer: 2000,
        });

        push(Paths.Users.Root);
      } catch (err) {
        show({
          type: 'error',
          title: 'Ooops...',
          text: 'Ocorreu um erro ao tentar cadastrar um novo usuário.\nTente novamente mais tarde.',
          showConfirmButton: true,
        });
      } finally {
        setLoading(false);
      }
    },
    [push],
  );

  const deleteUser = useCallback(
    async (uuid: string) => {
      try {
        setLoadingDelete(true);

        await deleteUserById(uuid);

        show({
          type: 'success',
          title: 'Usuário excluído com sucesso!',
          timer: 2500,
        }).then(async () => {
          await getUsers();
        });
      } catch (err) {
        await show({
          type: 'error',
          title: 'Ooops...',
          text: 'Ocorreu um erro ao tentar excluir o usuário.\nTente novamente mais tarde.',
          showConfirmButton: true,
        });
      } finally {
        setLoadingDelete(false);
      }
    },
    [getUsers],
  );

  const fetchUsersByFilters = useCallback(
    async (fullName?: string, login?: string, document?: string) => {
      try {
        setLoading(true);
        setUsers([]);
        setError(false);

        const response = await getUsersByFilters(fullName, login, document);

        setUsers(response.data);
      } catch (err) {
        setError(true);
      } finally {
        setLoading(false);
      }
    },
    [],
  );

  return {
    getUsers,
    getOneUser,
    createUser,
    updateUser,
    deleteUser,
    fetchUsersByFilters,
    loading,
    loadingDelete,
    error,
    users,
    user,
  };
};

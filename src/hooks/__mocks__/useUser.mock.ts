const usersMock = [
  {
    id: 1,
    uuid: 'adbe514b-ee76-4c4f-bb5c-d42819e6a7b8',
    login: 'fq_soares',
    fullName: 'Fernando Henrique Ferreira Soares',
    document: '32165498710',
    active: true,
    createdAt: '2021-06-26T14:22:15.220Z',
    updatedAt: '2021-06-26T14:22:15.220Z',
    address: {
      zipCode: '14412206',
      street: 'Rua Jovita',
      number: 462,
      district: 'Jardim das Palmeiras',
      city: 'Franca',
      state: 'SP',
    },
  },
  {
    id: 2,
    uuid: 'a0e85b18-2d17-4b0b-b4b5-ae536df85443',
    login: 'lu_bomfim',
    fullName: 'Luis Fernando Bomfim',
    document: '12345678901',
    active: true,
    createdAt: '2021-06-26T14:22:15.220Z',
    updatedAt: '2021-06-26T14:22:15.220Z',
    address: {
      zipCode: '14403500',
      street: 'Avenida Santa Cruz',
      number: 3358,
      district: 'Jardim das Oliveiras',
      city: 'Franca',
      state: 'SP',
    },
  },
];

const userRequestMock = {
  login: 'fq_soares',
  fullName: 'Fernando Henrique Ferreira Soares',
  document: '32165498710',
  address: {
    zipCode: '14412206',
    street: 'Rua Jovita',
    number: 462,
    district: 'Jardim das Palmeiras',
    city: 'Franca',
    state: 'SP',
  },
};

export { usersMock, userRequestMock };

const Paths = {
  Home: '/',
  Users: {
    Root: '/users',
    New: '/users/new',
    Edit: '/users/:uuid/edit',
  },
};

export default Paths;

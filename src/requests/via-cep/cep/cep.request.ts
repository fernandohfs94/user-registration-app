import { AxiosResponse } from 'axios';

import ViaCepAPI from '../base.request';

import { ICepResponse } from './interfaces/cep.interface';

export const getAddressByZipCode = (
  zipCode: string,
): Promise<AxiosResponse<ICepResponse>> => {
  return ViaCepAPI.request<ICepResponse>({
    method: 'GET',
    url: `/${zipCode}/json`,
  });
};

import axios from 'axios';

import Env from '../../config/env.config';

const baseURL = Env.VIA_CEP_URL;
const timeout = Env.VIA_CEP_TIMEOUT;

const ViaCepAPI = axios.create({
  baseURL,
  timeout,
});

export default ViaCepAPI;

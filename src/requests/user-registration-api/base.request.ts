import axios from 'axios';

import Env from '../../config/env.config';

const baseURL = Env.USER_REGISTRATION_API_URL;
const token = Env.USER_REGISTRATION_API_TOKEN;
const timeout = Env.USER_REGISTRATION_API_TIMEOUT;

const API = axios.create({
  baseURL,
  headers: { Authorization: token },
  timeout,
});

export default API;

export interface IUserRequest {
  login: string;
  password: string;
  fullName: string;
  document: string;
  address: {
    zipCode: string;
    street: string;
    number: number;
    district: string;
    city: string;
    state: string;
  };
}
export interface IUserResponse {
  id: number;
  uuid: string;
  login: string;
  fullName: string;
  document: string;
  active: boolean;
  createdAt: Date;
  updatedAt: Date;
  address: {
    zipCode: string;
    street: string;
    number: number;
    district: string;
    city: string;
    state: string;
  };
}

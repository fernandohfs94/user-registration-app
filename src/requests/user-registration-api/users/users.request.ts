import { AxiosResponse } from 'axios';

import API from '../base.request';

import { IUserRequest, IUserResponse } from './interfaces/users.interface';

export const getAllUsers = (): Promise<AxiosResponse<IUserResponse[]>> => {
  return API.request<IUserResponse[]>({ method: 'GET', url: '/users' });
};

export const getUsersByFilters = (
  fullName?: string,
  login?: string,
  document?: string,
): Promise<AxiosResponse<IUserResponse[]>> => {
  let query = '';

  if (fullName && login && document) {
    query += `?fullName=${fullName}&login=${login}&document=${document}`;
  } else if (fullName && !login && !document) {
    query += `?fullName=${fullName}`;
  } else if (!fullName && login && !document) {
    query += `?login=${login}`;
  } else if (!fullName && !login && document) {
    query += `?document=${document}`;
  } else if (fullName && login && !document) {
    query += `?fullName=${fullName}&login=${login}`;
  } else if (fullName && !login && document) {
    query += `?fullName=${fullName}&document=${document}`;
  } else if (!fullName && login && document) {
    query += `?login=${login}&document=${document}`;
  }

  return API.request<IUserResponse[]>({
    method: 'GET',
    url: `/users${query}`,
  });
};

export const getUserByUuid = (
  uuid: string,
): Promise<AxiosResponse<IUserResponse>> => {
  return API.request<IUserResponse>({ method: 'GET', url: `/users/${uuid}` });
};

export const addNewUser = (
  data: IUserRequest,
): Promise<AxiosResponse<IUserResponse>> => {
  return API.request<IUserResponse>({ method: 'POST', url: '/users', data });
};

export const updateUserByUuid = (
  data: IUserRequest,
  uuid: string,
): Promise<AxiosResponse<IUserResponse>> => {
  return API.request<IUserResponse>({
    method: 'PATCH',
    url: `/users/${uuid}`,
    data,
  });
};

export const deleteUserById = (uuid: string): Promise<AxiosResponse> => {
  return API.request<IUserResponse>({
    method: 'DELETE',
    url: `/users/${uuid}`,
  });
};

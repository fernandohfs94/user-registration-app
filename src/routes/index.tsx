import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Paths from '../constants/paths.constant';

import Home from '../pages/Home';
import Users from '../pages/Users';
import UsersEdit from '../pages/Users/UsersEdit';
import UsersNew from '../pages/Users/UsersNew';

function Routes() {
  return (
    <Router>
      <Switch>
        <Route path={Paths.Home} component={Home} exact />
        <Route path={Paths.Users.Root} component={Users} exact />
        <Route path={Paths.Users.New} component={UsersNew} exact />
        <Route path={Paths.Users.Edit} component={UsersEdit} exact />
      </Switch>
    </Router>
  );
}

export default Routes;

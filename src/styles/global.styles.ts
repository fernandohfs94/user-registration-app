import { createGlobalStyle } from 'styled-components';

import Color from './color';

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    @media (min-width: 1080px) {
      font-size: 93.75%;
    }

    @media (min-width: 720px) {
      font-size: 87.5%;
    }
  }

  body {
    background: ${Color.background};
    -webkit-font-smoothing: antialiased;
  }

  html, body, #root {
    width: 100%;
    height: 100%;
  }

  body, input, textarea, button {
    font-family: 'Roboto', sans-serif;
    font-weight: 400;
  }

  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 700;
  }

  button {
    cursor: pointer;
  }

  [disabled] {
    opacity: 0.6;
    cursor: not-allowed;
  }

  a {
    color: ${Color.info.dark};
    text-decoration: none;
  }
`;

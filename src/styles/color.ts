const Color = {
  background: '#EDEDED',
  primary: '#0086ff',
  primary110: '#0079e6',
  primary120: '#0053cc',
  grey40: '#424242',
  grey30: '#a7a7a7',
  grey20: '#e5e5e5',
  grey10: '#f3f3f3',
  white: {
    default: '#ffffff',
    opacity: '#fff9',
  },
  info: {
    default: '#0086FF',
    light: '#E8F4FD',
    dark: '#004d92',
  },
  success: {
    default: '#08A93C',
    light: '#1BC250',
    dark: '#008F2D',
  },
  error: {
    default: '#E12A45',
    light: '#fdecea',
  },
  alert: {
    default: '#FFC700',
    dark: '#E4B50A',
  },
};

export default Color;

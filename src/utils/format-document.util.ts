export const formatDocument = (document: string): string => {
  return document.replace(/(\d{3})(\d{3})(\d{3})(\d{1,2})/g, '$1.$2.$3-$4');
};

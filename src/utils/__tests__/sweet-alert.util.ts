import Swal from 'sweetalert2';

import { show } from '../sweet-alert.util';

jest.mock('sweetalert2', () => {
  return {
    fire: jest.fn(),
  };
});

describe('Sweet Alert Util', () => {
  it('show() - should be able to show success sweetalert2 successfully', () => {
    show({ type: 'success', title: 'Testing' });

    expect(Swal.fire).toHaveBeenCalledWith({
      title: 'Testing',
      icon: 'success',
      text: '',
      showConfirmButton: false,
      showCancelButton: false,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      timer: 2000,
    });
  });

  it('show() - should be able to show error sweetalert2 successfully', () => {
    show({ type: 'error', title: 'Testing' });

    expect(Swal.fire).toHaveBeenCalledWith({
      title: 'Testing',
      icon: 'error',
      text: '',
      showConfirmButton: false,
      showCancelButton: false,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      timer: 2000,
    });
  });

  it('show() - should be able to show warning sweetalert2 successfully', () => {
    show({ type: 'warning', title: 'Testing' });

    expect(Swal.fire).toHaveBeenCalledWith({
      title: 'Testing',
      icon: 'warning',
      text: '',
      showConfirmButton: false,
      showCancelButton: false,
      confirmButtonText: 'OK',
      cancelButtonText: 'Cancelar',
      timer: 2000,
    });
  });
});

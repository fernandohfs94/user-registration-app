import { formatDocument } from '../format-document.util';

describe('Format document util', () => {
  it('should be able to format document successfully', () => {
    const document = '12345678901';
    const documentFormatted = formatDocument(document);

    expect(documentFormatted).toBe('123.456.789-01');
  });
});

import Swal, { SweetAlertResult } from 'sweetalert2';

interface AlertProps {
  type: 'success' | 'error' | 'warning';
  title: string;
  text?: string;
  showConfirmButton?: boolean;
  showCancelButton?: boolean;
  confirmButtonText?: string;
  cancelButtonText?: string;
  timer?: number;
}

export const show = async ({
  type,
  title,
  text = '',
  showConfirmButton = false,
  showCancelButton = false,
  confirmButtonText = 'OK',
  cancelButtonText = 'Cancelar',
  timer = 2000,
}: AlertProps): Promise<SweetAlertResult> => {
  return Swal.fire({
    icon: type,
    title,
    text,
    showConfirmButton,
    showCancelButton,
    confirmButtonText,
    cancelButtonText,
    timer,
  });
};

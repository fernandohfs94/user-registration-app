import styled from 'styled-components';
import { Col } from 'react-grid-system';

import Button from '../../components/Button';
import Color from '../../styles/color';

export const FormContainer = styled.div`
  margin-top: 1.5rem;
  padding: 1.5rem;
  background: ${Color.grey20};
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;

  fieldset {
    padding: 1.75rem 0;
    border: none;

    legend {
      font-size: 1.5rem;
      font-weight: 700;
    }
  }
`;

export const FormItem = styled(Col)`
  margin-bottom: 1rem;
`;

export const Label = styled.label`
  font-weight: 700;
`;

export const Input = styled.input`
  width: 100%;
  padding: 0.75rem 1rem;
  border-radius: 0.25rem;
  border: 1px solid ${Color.grey10};
  font-size: 1rem;
  outline: none;
  margin-top: 0.5rem;

  &:disabled {
    background: ${Color.white.default};
  }
`;

export const Select = styled.select`
  width: 100%;
  background: ${Color.white.default};
  padding: 0.75rem 1rem;
  border-radius: 0.25rem;
  border: 1px solid ${Color.grey10};
  font-size: 1rem;
  outline: none;
  margin-top: 0.5rem;
`;

export const TextError = styled.span`
  display: flex;
  font-size: 0.8rem;
  margin-top: 0.5rem;
  color: ${Color.error.default};
`;

export const RadioButtonBox = styled.div`
  height: 44px;
  margin-top: 0.5rem;

  display: flex;
  align-items: center;

  label {
    margin-right: 1.5rem;
  }
`;

export const FormButtonBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

export const FormButton = styled(Button)`
  margin-top: 0.5rem;
`;

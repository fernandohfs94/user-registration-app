import { mount } from 'enzyme';
import { render, fireEvent, screen, act } from '@testing-library/react';

import { useAddress } from '../../../../hooks/useAddress';
import { useUser } from '../../../../hooks/useUser';
import {
  inputValuesMock,
  searchAddressByZipCodeResultMock,
} from '../__mocks__/FormUsers.mock';

import FormUsers from '../index';

jest.mock('../../../../hooks/useAddress');
jest.mock('../../../../hooks/useUser');

describe('FormUsers container', () => {
  it('should be able to render FormUsers successfully', () => {
    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: jest.fn(),
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: jest.fn(),
      loading: false,
    });

    const wrapper = mount(<FormUsers />);

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to watch inputs correctly', () => {
    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: jest.fn(),
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: jest.fn(),
      loading: false,
    });

    const { container } = render(<FormUsers />);

    const inputLogin = container.querySelector(
      "input[name='login']",
    ) as HTMLInputElement;
    const inputPassword = container.querySelector(
      "input[name='password']",
    ) as HTMLInputElement;
    const inputDocument = container.querySelector(
      "input[name='document']",
    ) as HTMLInputElement;
    const inputFullName = container.querySelector(
      "input[name='fullName']",
    ) as HTMLInputElement;
    const inputAddressZipCode = container.querySelector(
      "input[name='address.zipCode']",
    ) as HTMLInputElement;
    const inputAddressStreet = container.querySelector(
      "input[name='address.street']",
    ) as HTMLInputElement;
    const inputAddressNumber = container.querySelector(
      "input[name='address.number']",
    ) as HTMLInputElement;
    const inputAddressDistrict = container.querySelector(
      "input[name='address.district']",
    ) as HTMLInputElement;
    const inputAddressCity = container.querySelector(
      "input[name='address.city']",
    ) as HTMLInputElement;
    const selectAddressState = container.querySelector(
      "select[name='address.state']",
    ) as HTMLInputElement;

    fireEvent.change(inputLogin, { target: { value: inputValuesMock.login } });
    fireEvent.change(inputPassword, {
      target: { value: inputValuesMock.password },
    });
    fireEvent.change(inputDocument, {
      target: { value: inputValuesMock.document },
    });
    fireEvent.change(inputFullName, {
      target: { value: inputValuesMock.fullName },
    });
    fireEvent.change(inputAddressZipCode, {
      target: { value: inputValuesMock.address.zipCode },
    });
    fireEvent.change(inputAddressStreet, {
      target: { value: inputValuesMock.address.street },
    });
    fireEvent.change(inputAddressNumber, {
      target: { value: inputValuesMock.address.number },
    });
    fireEvent.change(inputAddressDistrict, {
      target: { value: inputValuesMock.address.district },
    });
    fireEvent.change(inputAddressCity, {
      target: { value: inputValuesMock.address.city },
    });

    fireEvent.keyDown(selectAddressState, { keyCode: 40 });

    const testOption = container.querySelectorAll('option')[1];

    fireEvent.click(testOption);

    expect(inputLogin.value).toEqual(inputValuesMock.login);
    expect(inputPassword.value).toEqual(inputValuesMock.password);
    expect(inputDocument.value).toEqual(inputValuesMock.document);
    expect(inputFullName.value).toEqual(inputValuesMock.fullName);
    expect(inputAddressZipCode.value).toEqual(inputValuesMock.address.zipCode);
    expect(inputAddressStreet.value).toEqual(inputValuesMock.address.street);
    expect(inputAddressNumber.value).toEqual(
      inputValuesMock.address.number.toString(),
    );
    expect(inputAddressDistrict.value).toEqual(
      inputValuesMock.address.district,
    );
    expect(inputAddressCity.value).toEqual(inputValuesMock.address.city);
    expect(testOption.value).toEqual('TT');
  });

  it('should be able to display error messages when inputs are not filled', async () => {
    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: jest.fn(),
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: jest.fn(),
      loading: false,
    });

    const { container } = render(<FormUsers />);

    const submitButton = container.querySelector('button') as HTMLButtonElement;

    await act(async () => {
      fireEvent.submit(submitButton);
    });

    expect(screen.getAllByText('O campo login é obrigatório')).toBeTruthy();
    expect(screen.getAllByText('O campo senha é obrigatório')).toBeTruthy();
    expect(screen.getAllByText('O campo CPF é obrigatório')).toBeTruthy();
    expect(
      screen.getAllByText('O campo nome completo é obrigatório'),
    ).toBeTruthy();
    expect(screen.getAllByText('O campo CEP é obrigatório')).toBeTruthy();
    expect(screen.getAllByText('O campo rua é obrigatório')).toBeTruthy();
    expect(screen.getAllByText('O campo número é obrigatório')).toBeTruthy();
    expect(screen.getAllByText('O campo bairro é obrigatório')).toBeTruthy();
    expect(screen.getAllByText('O campo cidade é obrigatório')).toBeTruthy();
    expect(screen.getAllByText('O estado é obrigatório')).toBeTruthy();
  });

  it('should be able to search address by zip code successfully', () => {
    const searchAddressByZipCodeMock = jest
      .fn()
      .mockResolvedValue(searchAddressByZipCodeResultMock);

    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: searchAddressByZipCodeMock,
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: jest.fn(),
      loading: false,
    });

    const { container } = render(<FormUsers />);

    const inputAddressZipCode = container.querySelector(
      "input[name='address.zipCode']",
    ) as HTMLInputElement;

    fireEvent.change(inputAddressZipCode, {
      target: { value: inputValuesMock.address.zipCode },
    });
    fireEvent.focusOut(inputAddressZipCode);

    expect(searchAddressByZipCodeMock).toBeCalledTimes(1);
  });

  it('should be able to do nothing on search address by zip code when length of input is greater than 8', () => {
    const searchAddressByZipCodeMock = jest
      .fn()
      .mockResolvedValue(searchAddressByZipCodeResultMock);

    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: jest.fn(),
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: jest.fn(),
      loading: false,
    });

    const { container } = render(<FormUsers />);

    const inputAddressZipCode = container.querySelector(
      "input[name='address.zipCode']",
    ) as HTMLInputElement;

    fireEvent.change(inputAddressZipCode, {
      target: { value: '12345678901' },
    });
    fireEvent.focusOut(inputAddressZipCode);

    expect(searchAddressByZipCodeMock).not.toBeCalled();
  });

  it('should be able to add a new user succesfully', async () => {
    const createUserMock = jest.fn().mockResolvedValue('');

    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: jest.fn(),
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: createUserMock,
      loading: true,
    });

    const { container } = render(<FormUsers />);

    const inputLogin = container.querySelector(
      "input[name='login']",
    ) as HTMLInputElement;
    const inputPassword = container.querySelector(
      "input[name='password']",
    ) as HTMLInputElement;
    const inputDocument = container.querySelector(
      "input[name='document']",
    ) as HTMLInputElement;
    const inputFullName = container.querySelector(
      "input[name='fullName']",
    ) as HTMLInputElement;
    const inputAddressZipCode = container.querySelector(
      "input[name='address.zipCode']",
    ) as HTMLInputElement;
    const inputAddressStreet = container.querySelector(
      "input[name='address.street']",
    ) as HTMLInputElement;
    const inputAddressNumber = container.querySelector(
      "input[name='address.number']",
    ) as HTMLInputElement;
    const inputAddressDistrict = container.querySelector(
      "input[name='address.district']",
    ) as HTMLInputElement;
    const inputAddressCity = container.querySelector(
      "input[name='address.city']",
    ) as HTMLInputElement;
    const selectAddressState = container.querySelector(
      "select[name='address.state']",
    ) as HTMLInputElement;

    fireEvent.change(inputLogin, { target: { value: inputValuesMock.login } });
    fireEvent.change(inputPassword, {
      target: { value: inputValuesMock.password },
    });
    fireEvent.change(inputDocument, {
      target: { value: inputValuesMock.document },
    });
    fireEvent.change(inputFullName, {
      target: { value: inputValuesMock.fullName },
    });
    fireEvent.change(inputAddressZipCode, {
      target: { value: inputValuesMock.address.zipCode },
    });
    fireEvent.change(inputAddressStreet, {
      target: { value: inputValuesMock.address.street },
    });
    fireEvent.change(inputAddressNumber, {
      target: { value: inputValuesMock.address.number },
    });
    fireEvent.change(inputAddressDistrict, {
      target: { value: inputValuesMock.address.district },
    });
    fireEvent.change(inputAddressCity, {
      target: { value: inputValuesMock.address.city },
    });
    fireEvent.change(selectAddressState, {
      target: { value: 'TT' },
    });

    const submitButton = container.querySelector('button') as HTMLButtonElement;

    await act(async () => {
      fireEvent.submit(submitButton);
    });

    expect(createUserMock).toBeCalledTimes(1);
  });

  it('should be able to render FormUsers with initalData and uuid successfully', () => {
    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: jest.fn(),
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: jest.fn(),
      loading: false,
    });

    const wrapper = mount(
      <FormUsers
        initialData={inputValuesMock}
        uuid="adbe514b-ee76-4c4f-bb5c-d42819e6a7b8"
      />,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to call updateUser function when initialData and uuid are entered', async () => {
    const updateUserMock = jest.fn();

    (useAddress as unknown as jest.Mock).mockReturnValue({
      getStates: jest.fn(),
      searchAddressByZipCode: jest.fn(),
      states: [{ description: 'Test', value: 'TT' }],
    });
    (useUser as unknown as jest.Mock).mockReturnValue({
      createUser: jest.fn(),
      updateUser: updateUserMock,
      loading: false,
    });

    const { container } = render(
      <FormUsers
        initialData={inputValuesMock}
        uuid="adbe514b-ee76-4c4f-bb5c-d42819e6a7b8"
      />,
    );

    const inputPassword = container.querySelector(
      "input[name='password']",
    ) as HTMLInputElement;
    const inputFullName = container.querySelector(
      "input[name='fullName']",
    ) as HTMLInputElement;
    const inputAddressZipCode = container.querySelector(
      "input[name='address.zipCode']",
    ) as HTMLInputElement;
    const inputAddressStreet = container.querySelector(
      "input[name='address.street']",
    ) as HTMLInputElement;
    const inputAddressNumber = container.querySelector(
      "input[name='address.number']",
    ) as HTMLInputElement;
    const inputAddressDistrict = container.querySelector(
      "input[name='address.district']",
    ) as HTMLInputElement;
    const inputAddressCity = container.querySelector(
      "input[name='address.city']",
    ) as HTMLInputElement;
    const selectAddressState = container.querySelector(
      "select[name='address.state']",
    ) as HTMLInputElement;

    fireEvent.change(inputPassword, {
      target: { value: 'newPassword' },
    });
    fireEvent.change(inputFullName, {
      target: { value: 'newName' },
    });
    fireEvent.change(inputAddressZipCode, {
      target: { value: inputValuesMock.address.zipCode },
    });
    fireEvent.change(inputAddressStreet, {
      target: { value: inputValuesMock.address.street },
    });
    fireEvent.change(inputAddressNumber, {
      target: { value: inputValuesMock.address.number },
    });
    fireEvent.change(inputAddressDistrict, {
      target: { value: inputValuesMock.address.district },
    });
    fireEvent.change(inputAddressCity, {
      target: { value: inputValuesMock.address.city },
    });
    fireEvent.change(selectAddressState, {
      target: { value: 'TT' },
    });

    const submitButton = container.querySelector('button') as HTMLButtonElement;

    await act(async () => {
      fireEvent.submit(submitButton);
    });

    expect(updateUserMock).toBeCalledTimes(1);
  });
});

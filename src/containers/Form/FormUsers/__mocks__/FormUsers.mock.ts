const inputValuesMock = {
  login: 'fq_soares',
  password: '123456',
  fullName: 'Fernando Henrique Ferreira Soares',
  document: '12345678901',
  address: {
    zipCode: '14412206',
    street: 'Rua Jovita',
    number: 462,
    district: 'Jardim das Palmeiras',
    city: 'Franca',
    state: 'SP',
  },
};

const searchAddressByZipCodeResultMock = {
  cep: '14412-206',
  logradouro: 'Rua Jovita Maria da Conceição',
  complemento: '',
  bairro: 'Conjunto Habitacional Octávio Cilurzo',
  localidade: 'Franca',
  uf: 'SP',
  ibge: '3516200',
  gia: '3104',
  ddd: '16',
  siafi: '6425',
};

export { inputValuesMock, searchAddressByZipCodeResultMock };

/* eslint-disable react/jsx-props-no-spreading */
import { useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Col, Row } from 'react-grid-system';

import Loading from '../../../components/Loading';

import { useAddress } from '../../../hooks/useAddress';
import { useUser } from '../../../hooks/useUser';

import {
  Form,
  FormButton,
  FormContainer,
  FormItem,
  Input,
  Label,
  Select,
  TextError,
} from '../styles';

type AddressData = {
  zipCode: string;
  street: string;
  number: number;
  district: string;
  city: string;
  state: string;
};

type FormData = {
  login: string;
  password?: string;
  fullName: string;
  document: string;
  address: AddressData;
};

interface FormUsersProp {
  initialData?: FormData;
  uuid?: string;
}

function FormUsers({ initialData, uuid }: FormUsersProp) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm<FormData>({
    defaultValues: initialData,
  });
  const { getStates, searchAddressByZipCode, states } = useAddress();
  const { createUser, updateUser, loading } = useUser();

  useEffect(() => {
    getStates();
  }, [getStates]);

  const handleZipCodeBlur = useCallback(
    async event => {
      const { value } = event.target;

      const zipCode = value?.replace(/[^0-9]/g, '');

      if (zipCode?.length !== 8) return;

      searchAddressByZipCode(zipCode).then(address => {
        setValue('address.street', address.logradouro);
        setValue('address.district', address.bairro);
        setValue('address.city', address.localidade);
        setValue('address.state', address.uf);
      });
    },
    [searchAddressByZipCode, setValue],
  );

  const handleFormSubmit = useCallback(
    async data => {
      if (initialData && uuid) {
        await updateUser(data, uuid);
      } else {
        await createUser(data);
      }
    },
    [createUser, updateUser, initialData, uuid],
  );

  return (
    <>
      {loading && <Loading />}

      <FormContainer>
        <Form onSubmit={handleSubmit(handleFormSubmit)}>
          <fieldset>
            <legend>Informações Pessoais:</legend>
            <Row>
              <FormItem xs={12} md={4}>
                <Label>Login:</Label>
                <Input
                  disabled={!!initialData?.login}
                  {...register('login', {
                    required: {
                      value: true,
                      message: 'O campo login é obrigatório',
                    },
                    maxLength: {
                      value: 20,
                      message: 'Permitido apenas 20 caracteres',
                    },
                  })}
                />
                {errors.login && <TextError>{errors.login.message}</TextError>}
              </FormItem>

              <FormItem xs={12} md={4}>
                <Label>Senha:</Label>
                <Input
                  type="password"
                  {...register('password', {
                    required: {
                      value: true,
                      message: 'O campo senha é obrigatório',
                    },
                    maxLength: {
                      value: 100,
                      message: 'Permitido apenas 100 caracteres',
                    },
                  })}
                />
                {errors.password && (
                  <TextError>{errors.password.message}</TextError>
                )}
              </FormItem>
            </Row>

            <Row>
              <FormItem xs={12} md={4}>
                <Label>CPF:</Label>
                <Input
                  disabled={!!initialData?.login}
                  {...register('document', {
                    required: {
                      value: true,
                      message: 'O campo CPF é obrigatório',
                    },
                    minLength: {
                      value: 11,
                      message: 'Digite um CPF válido',
                    },
                    maxLength: {
                      value: 11,
                      message: 'Permitido apenas 11 caracteres',
                    },
                    pattern: {
                      value: /\d/g,
                      message: 'Apenas números',
                    },
                  })}
                />
                {errors.document && (
                  <TextError>{errors.document.message}</TextError>
                )}
              </FormItem>

              <FormItem xs={12} md={4}>
                <Label>Nome completo:</Label>
                <Input
                  {...register('fullName', {
                    required: {
                      value: true,
                      message: 'O campo nome completo é obrigatório',
                    },
                    maxLength: {
                      value: 100,
                      message: 'Permitido apenas 100 caracteres',
                    },
                  })}
                />
                {errors.fullName && (
                  <TextError>{errors.fullName.message}</TextError>
                )}
              </FormItem>
            </Row>
          </fieldset>

          <fieldset>
            <legend>Endereço</legend>

            <Row>
              <FormItem xs={12} md={4}>
                <Label>CEP:</Label>
                <Input
                  {...register('address.zipCode', {
                    required: {
                      value: true,
                      message: 'O campo CEP é obrigatório',
                    },
                    minLength: {
                      value: 8,
                      message: 'Digite um CEP válido',
                    },
                    maxLength: {
                      value: 8,
                      message: 'Permitido apenas 8 caracteres',
                    },
                    pattern: {
                      value: /\d/g,
                      message: 'Apenas números',
                    },
                  })}
                  onBlur={event => handleZipCodeBlur(event)}
                />
                {errors.address?.zipCode && (
                  <TextError>{errors.address?.zipCode.message}</TextError>
                )}
              </FormItem>

              <FormItem xs={12} md={4}>
                <Label>Rua:</Label>
                <Input
                  {...register('address.street', {
                    required: {
                      value: true,
                      message: 'O campo rua é obrigatório',
                    },
                  })}
                />
                {errors.address?.street && (
                  <TextError>{errors.address?.street.message}</TextError>
                )}
              </FormItem>

              <FormItem xs={12} md={4}>
                <Label>Número:</Label>
                <Input
                  type="number"
                  {...register('address.number', {
                    required: {
                      value: true,
                      message: 'O campo número é obrigatório',
                    },
                    pattern: {
                      value: /\d/g,
                      message: 'Apenas números',
                    },
                  })}
                />
                {errors.address?.number && (
                  <TextError>{errors.address?.number.message}</TextError>
                )}
              </FormItem>
            </Row>

            <Row>
              <FormItem xs={12} md={4}>
                <Label>Bairro:</Label>
                <Input
                  {...register('address.district', {
                    required: {
                      value: true,
                      message: 'O campo bairro é obrigatório',
                    },
                    maxLength: {
                      value: 200,
                      message: 'Permitido apenas 200 caracteres',
                    },
                  })}
                />
                {errors.address?.district && (
                  <TextError>{errors.address?.district.message}</TextError>
                )}
              </FormItem>

              <FormItem xs={12} md={4}>
                <Label>Cidade:</Label>
                <Input
                  {...register('address.city', {
                    required: {
                      value: true,
                      message: 'O campo cidade é obrigatório',
                    },
                    maxLength: {
                      value: 100,
                      message: 'Permitido apenas 100 caracteres',
                    },
                  })}
                />
                {errors.address?.city && (
                  <TextError>{errors.address?.city.message}</TextError>
                )}
              </FormItem>

              <FormItem xs={12} md={4}>
                <Label>Estado:</Label>
                <Select
                  value={initialData?.address?.state}
                  {...register('address.state', {
                    required: {
                      value: true,
                      message: 'O estado é obrigatório',
                    },
                  })}
                >
                  <option value="">Selecione...</option>
                  {states.map(({ value, description }) => (
                    <option key={value} value={value}>
                      {description}
                    </option>
                  ))}
                </Select>
                {errors.address?.state && (
                  <TextError>{errors.address?.state.message}</TextError>
                )}
              </FormItem>
            </Row>
          </fieldset>

          <Row>
            <Col xs={12} md={4}>
              <FormButton type="submit" fluid>
                {initialData && uuid ? 'Atualizar' : 'Cadastrar'}
              </FormButton>
            </Col>
          </Row>
        </Form>
      </FormContainer>
    </>
  );
}

export default FormUsers;

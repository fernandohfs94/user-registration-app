import { mount } from 'enzyme';

import Layout from '../index';

describe('Layout container', () => {
  it('should be able to render Layout container successfully', () => {
    const wrapper = mount(
      <Layout>
        <p>Testing</p>
      </Layout>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

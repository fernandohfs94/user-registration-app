import { mount } from 'enzyme';

import Strip from '../index';

describe('Strip component', () => {
  it('should be able to render Strip component successfully', () => {
    const wrapper = mount(<Strip />);

    expect(wrapper).toMatchSnapshot();
  });
});

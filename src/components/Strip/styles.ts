import styled from 'styled-components';

export const Container = styled.div`
  position: relative;
  height: 5px;
  border-radius: 2px 2px 0 0;
  background: linear-gradient(
    to right,
    #fcd000 0,
    #ff8a00 17%,
    #ff253a 34%,
    #ff37a8 51%,
    #a400e1 67%,
    #0086ff 83%,
    #00d604 100%
  );
`;

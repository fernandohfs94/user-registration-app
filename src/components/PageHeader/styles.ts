import styled from 'styled-components';

import Color from '../../styles/color';

export const Container = styled.div`
  hr {
    margin: 1rem 0;
    border: 0;
    border-top: 1px solid ${Color.grey40};
  }
`;

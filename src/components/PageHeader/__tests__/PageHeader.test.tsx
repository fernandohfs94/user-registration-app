import { mount } from 'enzyme';

import PageHeader from '../index';

describe('PageHeader component', () => {
  it('should be able to render PageHeader component successfully', () => {
    const wrapper = mount(<PageHeader title="Testing" />);

    expect(wrapper).toMatchSnapshot();
  });
});

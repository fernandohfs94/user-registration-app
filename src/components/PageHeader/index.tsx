import { Container } from './styles';

interface PageHeaderProps {
  title: string;
}

function PageHeader({ title }: PageHeaderProps) {
  return (
    <Container>
      <h1>{title}</h1>
      <hr />
    </Container>
  );
}

export default PageHeader;

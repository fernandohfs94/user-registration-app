import styled from 'styled-components';

export const Container = styled.div`
  margin-top: 2rem;
  margin-bottom: 4.5rem;
`;

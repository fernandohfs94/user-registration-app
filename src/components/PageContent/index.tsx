import { ReactNode } from 'react';

import { Container } from './styles';

interface PageContentProps {
  children: ReactNode;
}

function PageContent({ children }: PageContentProps) {
  return <Container>{children}</Container>;
}

export default PageContent;

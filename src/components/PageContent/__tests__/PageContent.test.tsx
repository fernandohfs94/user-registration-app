import { mount } from 'enzyme';

import PageContent from '../index';

describe('PageContent component', () => {
  it('should be able to render PageContent component successfully', () => {
    const wrapper = mount(
      <PageContent>
        <p>Testing</p>
      </PageContent>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

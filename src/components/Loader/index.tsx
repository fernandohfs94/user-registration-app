import React from 'react';

import { Container, Spinner } from './styles';

interface LoaderProps {
  text?: string;
}

function Loader({ text }: LoaderProps) {
  return (
    <Container>
      <Spinner />
      {!!text && <p>{text}</p>}
    </Container>
  );
}

export default Loader;

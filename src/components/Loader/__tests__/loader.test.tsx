import { mount } from 'enzyme';

import Loader from '../index';

describe('Loader component', () => {
  it('should be able to render Loader with text successfully', () => {
    const wrapper = mount(<Loader text="Loading test..." />);

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render Loader without text successfully', () => {
    const wrapper = mount(<Loader />);

    expect(wrapper).toMatchSnapshot();
  });
});

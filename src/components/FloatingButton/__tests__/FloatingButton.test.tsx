import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import FloatingButton from '../index';

describe('FloatingButton component', () => {
  it('should be able to render FloatingButton successfully', () => {
    const wrapper = mount(
      <Router>
        <FloatingButton path="/testing" />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

import { FaPlus } from 'react-icons/fa';

import { Container } from './styles';

interface FloatingButtonProps {
  path: string;
}

function FloatingButton({ path }: FloatingButtonProps) {
  return (
    <Container to={path}>
      <FaPlus size={20} />
    </Container>
  );
}

export default FloatingButton;

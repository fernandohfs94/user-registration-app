import styled from 'styled-components';
import { Link } from 'react-router-dom';

import Color from '../../styles/color';

export const Container = styled(Link)`
  position: fixed;
  bottom: 2.5rem;
  right: 2.5rem;
  padding: 1.25rem;
  background: ${Color.success.dark};
  border-radius: 50%;
  color: ${Color.white.default};
  box-shadow: 2px 2px 3px rgba(0, 0, 0, 0.15);
  transition: opacity 0.5s;

  display: flex;
  justify-content: center;
  align-items: center;

  @media (max-width: 420px) {
    bottom: 1.5rem;
    right: 1rem;
  }

  &:hover {
    opacity: 0.8;
  }
`;

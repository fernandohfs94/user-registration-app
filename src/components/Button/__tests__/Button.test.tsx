import { mount } from 'enzyme';

import Button from '../index';

describe('Button component', () => {
  it('should be able to render Button component successfully', () => {
    const wrapper = mount(<Button>Testing</Button>);

    expect(wrapper).toMatchSnapshot();
  });
});

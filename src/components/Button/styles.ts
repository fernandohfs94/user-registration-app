import styled, { css } from 'styled-components';
import { ButtonHTMLAttributes } from 'react';

import Color from '../../styles/color';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  fluid?: boolean;
}

export const Container = styled.button<ButtonProps>`
  width: 100%;
  height: 47px;
  background: ${Color.success.default};
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.15);
  border-radius: 0.375rem;
  border: none;
  padding: 0 1rem;
  color: ${Color.white.default};
  font-weight: 700;
  font-size: 1rem;
  margin-top: 1rem;
  transition: background-color 0.2s;
  cursor: pointer;
  outline: 0;

  &:hover {
    background: ${Color.success.dark};
  }

  ${p =>
    !p.fluid &&
    css`
      @media (min-width: 320px) {
        max-width: 350px;
      }
    `}
`;

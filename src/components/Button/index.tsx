import { ButtonHTMLAttributes, ReactNode } from 'react';

import { Container } from './styles';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  fluid?: boolean;
  children: ReactNode;
}

function Button({ fluid = false, children, ...rest }: ButtonProps) {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Container type="button" fluid={fluid} {...rest}>
      {children}
    </Container>
  );
}

export default Button;

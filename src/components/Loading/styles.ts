import styled, { keyframes } from 'styled-components';

import Color from '../../styles/color';

export const Container = styled.div`
  content: '';
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: ${Color.white.opacity};
  z-index: 999999;
  position: fixed;
  height: 100%;
  width: 100%;
  display: block;
`;

const SpinnerAnimation = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

export const Spinner = styled.div`
  position: fixed;
  z-index: 999999;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: 50px;
  height: 50px;
  overflow: visible;
  margin: auto;
  border-radius: 80%;
  border-bottom: 13px solid ${Color.error.default};
  border-top: 13px solid ${Color.primary110};
  border-left: 13px solid ${Color.success.dark};
  border-right: 13px solid ${Color.alert.dark};
  -webkit-animation: ${SpinnerAnimation} 2s linear infinite;
  animation: ${SpinnerAnimation} 0.5s linear infinite;
  box-shadow: 0px 0.3px 4px 0px black;
`;

import { mount } from 'enzyme';

import Loading from '../index';

describe('Loading component', () => {
  it('should be able to render Loading component succesfully', () => {
    const wrapper = mount(<Loading />);

    expect(wrapper).toMatchSnapshot();
  });
});

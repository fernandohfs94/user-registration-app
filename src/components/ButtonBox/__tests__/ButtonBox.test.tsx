import { mount } from 'enzyme';

import ButtonBox from '../index';

describe('ButtonBox component', () => {
  it('should be able to render ButtonBox component successfully', () => {
    const wrapper = mount(
      <ButtonBox>
        <button type="button">Testing</button>
      </ButtonBox>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

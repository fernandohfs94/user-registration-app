import { ReactNode } from 'react';

import { Container } from './styles';

interface ButtonBoxProps {
  children: ReactNode;
}

function ButtonBox({ children }: ButtonBoxProps) {
  return <Container>{children}</Container>;
}

export default ButtonBox;

import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: center;
  height: 100%;

  button {
    margin-top: 0;
    height: 44px;
  }

  @media (max-width: 767px) {
    button {
      margin-top: 0.5rem;
    }
  }
`;

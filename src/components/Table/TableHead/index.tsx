import { ReactNode } from 'react';

interface TableHeadProps {
  children: ReactNode;
}

function TableHead({ children }: TableHeadProps) {
  return <thead>{children}</thead>;
}

export default TableHead;

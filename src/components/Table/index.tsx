import { ReactNode } from 'react';

import TableBody from './TableBody';
import TableCell from './TableCell';
import TableHead from './TableHead';
import TableHeadCell from './TableHeadCell';
import TableRow from './TableRow';

import { Container } from './styles';

interface TableProps {
  children: ReactNode;
}

function Table({ children }: TableProps) {
  return (
    <Container>
      <table>{children}</table>
    </Container>
  );
}

Table.Body = TableBody;
Table.Cell = TableCell;
Table.Head = TableHead;
Table.HeadCell = TableHeadCell;
Table.Row = TableRow;

export default Table;

import { ReactNode } from 'react';

import { Container } from './styles';

interface TableCellProps {
  children: ReactNode;
}

function TableCell({ children }: TableCellProps) {
  return <Container>{children}</Container>;
}

export default TableCell;

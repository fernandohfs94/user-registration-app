import styled from 'styled-components';

import Color from '../../../styles/color';

export const Container = styled.td`
  padding: 1rem 0.5rem;
  border: 1px solid ${Color.grey20};
  text-align: center;

  .action-icon + .action-icon {
    margin-left: 24px;
  }

  @media (max-width: 991px) {
    .action-icon {
      display: block;
    }

    .action-icon + .action-icon {
      margin: 1.5rem auto 0;
    }
  }
`;

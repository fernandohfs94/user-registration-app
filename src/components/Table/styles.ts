import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  overflow-x: auto;
  margin-top: 1.5rem;

  table {
    border-collapse: collapse;
    width: 100%;
  }
`;

import styled from 'styled-components';

import Color from '../../../styles/color';

export const Container = styled.tr`
  &:nth-child(even) {
    background: ${Color.white.default};
  }

  &:hover {
    background: ${Color.grey30};
  }
`;

import { ReactNode } from 'react';

import { Container } from './styles';

interface TableRowProps {
  children: ReactNode;
}

function TableRow({ children }: TableRowProps) {
  return <Container>{children}</Container>;
}

export default TableRow;

import { ReactNode } from 'react';

import { Container } from './styles';

interface TableHeadCellProps {
  children: ReactNode;
}

function TableHeadCell({ children }: TableHeadCellProps) {
  return <Container>{children}</Container>;
}

export default TableHeadCell;

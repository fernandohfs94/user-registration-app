import styled from 'styled-components';

import Color from '../../../styles/color';

export const Container = styled.th`
  padding: 1rem 0.5rem;
  text-align: center;
  background-color: ${Color.primary110};
  color: ${Color.white.default};
  border: 1px solid ${Color.grey20};
  font-weight: 700;

  &:first-child {
    border-top-left-radius: 8px;
  }

  &:last-child {
    border-top-right-radius: 8px;
  }
`;

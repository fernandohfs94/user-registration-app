import { mount } from 'enzyme';

import Table from '../index';

describe('Table component', () => {
  it('should be able to render complete Table component with success', () => {
    const wrapper = mount(
      <Table>
        <Table.Head>
          <Table.Row>
            <Table.HeadCell>Testing</Table.HeadCell>
          </Table.Row>
        </Table.Head>
        <Table.Body>
          <Table.Row>
            <Table.Cell>I am testing</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

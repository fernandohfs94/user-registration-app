import { ReactNode } from 'react';

import { Container } from './styles';

interface BreadcrumbsProps {
  children: ReactNode;
}

function Breadcrumbs({ children }: BreadcrumbsProps) {
  return <Container>{children}</Container>;
}

export default Breadcrumbs;

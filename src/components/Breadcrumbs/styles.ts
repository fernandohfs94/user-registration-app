import styled from 'styled-components';

import Color from '../../styles/color';

export const Container = styled.ul`
  padding: 0.5rem 1rem;
  background: ${Color.grey10};
  border-radius: 0.375rem;
`;

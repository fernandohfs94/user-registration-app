import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import BreadcrumbItem from '../BreadcrumbItem';

import Breadcrumbs from '../index';

describe('Breadcrumbs component', () => {
  it('should be able to render Breadcrumbs component successfully', () => {
    const wrapper = mount(
      <Router>
        <Breadcrumbs>
          <BreadcrumbItem path="/test" title="Testing" />
        </Breadcrumbs>
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

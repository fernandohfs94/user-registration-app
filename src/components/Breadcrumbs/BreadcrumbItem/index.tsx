import { Link } from 'react-router-dom';
import { Container } from './styles';

interface BreadcrumbItemProps {
  path?: string;
  title: string;
}

function BreadcrumbItem({ path, title }: BreadcrumbItemProps) {
  return <Container>{path ? <Link to={path}>{title}</Link> : title}</Container>;
}

export default BreadcrumbItem;

import styled from 'styled-components';

import Color from '../../../styles/color';

export const Container = styled.li`
  display: inline-block;
  font-size: 1.2rem;
  padding: 0.5rem 0;

  a {
    color: ${Color.primary};
    text-decoration: none;
  }

  a:hover {
    color: ${Color.primary110};
  }

  & + &:before {
    padding: 0.5rem;
    content: '/';
  }
`;

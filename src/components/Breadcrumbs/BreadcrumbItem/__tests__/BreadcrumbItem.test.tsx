import { BrowserRouter as Router } from 'react-router-dom';
import { mount } from 'enzyme';

import BreadcrumbItem from '../index';

describe('BreadcrumbItem component', () => {
  it('should be able to render BreadcrumbItem component successfully', () => {
    const wrapper = mount(
      <Router>
        <BreadcrumbItem path="/test" title="Testing" />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render BreadcrumbItem component without path successfully', () => {
    const wrapper = mount(
      <Router>
        <BreadcrumbItem title="Testing" />
      </Router>,
    );

    expect(wrapper).toMatchSnapshot();
  });
});

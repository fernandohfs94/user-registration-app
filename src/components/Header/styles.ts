import styled from 'styled-components';

import Color from '../../styles/color';

export const Container = styled.header`
  background: ${Color.primary};
  width: 100%;
  height: 4.5rem;
  display: flex;
  padding: 0.5rem 0;
`;

export const Navbar = styled.nav`
  width: 100%;
  max-width: 1440px;
  margin: 0 auto;
  padding: 0 2.5rem;

  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 420px) {
    padding: 0 2rem;
  }
`;

export const Logo = styled.img`
  max-height: 32px;
`;

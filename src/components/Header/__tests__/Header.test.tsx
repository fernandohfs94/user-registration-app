import { mount } from 'enzyme';

import Header from '../index';

describe('Header component', () => {
  it('should be able to render Header component successfully', () => {
    const wrapper = mount(<Header />);

    expect(wrapper).toMatchSnapshot();
  });
});

import Strip from '../Strip';

import LogoMagaluWhite from '../../assets/logo-magalu-white.png';

import { Container, Navbar, Logo } from './styles';

function Header() {
  return (
    <>
      <Strip />
      <Container>
        <Navbar>
          <Logo src={LogoMagaluWhite} alt="Magalu" />
        </Navbar>
      </Container>
    </>
  );
}

export default Header;

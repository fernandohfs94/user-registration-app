import { ReactNode } from 'react';

import { Container } from './styles';

interface AlertProps {
  type?: 'info' | 'success' | 'error';
  children: ReactNode;
}

function Alert({ type = 'info', children }: AlertProps) {
  return (
    <Container type={type}>
      <h3>{children}</h3>
    </Container>
  );
}

export default Alert;

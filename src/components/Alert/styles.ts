import styled from 'styled-components';

import Color from '../../styles/color';

interface ContainerProps {
  type: 'info' | 'success' | 'error';
}

export const Container = styled.div<ContainerProps>`
  margin: 1.5rem 0;
  padding: 1.5rem 1rem;
  text-align: center;

  display: flex;
  justify-content: center;

  background: ${p => Color[p.type].light};
  color: ${p => Color[p.type].default};

  h3 {
    line-height: 1.5rem;
  }
`;

import { mount } from 'enzyme';

import Alert from '../index';

describe('Alert component', () => {
  it('should be able to render Alert with default type successfully', () => {
    const wrapper = mount(<Alert>Testing</Alert>);

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render Alert with info type successfully', () => {
    const wrapper = mount(<Alert type="info">Testing</Alert>);

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render Alert with success type successfully', () => {
    const wrapper = mount(<Alert type="success">Testing</Alert>);

    expect(wrapper).toMatchSnapshot();
  });

  it('should be able to render Alert with error type successfully', () => {
    const wrapper = mount(<Alert type="error">Testing</Alert>);

    expect(wrapper).toMatchSnapshot();
  });
});
